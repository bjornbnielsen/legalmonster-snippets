<script type="text/javascript">
    !function(){var legal=window.legal=window.legal||[];if(!legal.__VERSION__)if(legal.invoked)window.console&&console.error&&console.error("Legal.js snippet included twice on page.");else{legal.invoked=!0,legal.methods=["document","signup","user","ensureConsent"],legal.factory=function(o){return function(){var e=Array.prototype.slice.call(arguments);return e.unshift(o),legal.push(e),legal}};for(var e=0;e<legal.methods.length;e++){var o=legal.methods[e];legal[o]=legal.factory(o)}legal.load=function(e,o){var t=document.createElement("script");t.type="text/javascript",t.async=!1,t.src="https://widgets.legalmonster.com/v1/legal.js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(t,n),legal.__project=e,legal.__loadOptions=o||{}},legal.SNIPPET_VERSION="1.1.0"}}();

    legal.load("<<PROJECT PUBLIC KEY>>");

    // At this point, you can make further calls on the legal object
</script>